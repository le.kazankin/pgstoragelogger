# PGStorageLogger

API внутреннего использование в Py программах для логирования операций с записью в базу данных PostgreSQL

## Использоваение


```
pip3 install psycopg2
Скопировать pgstoragelogger.py в папку с Вашим проектом.
import pgstoragelogger
logger = pgstoragelogger.PGStorageLogger('db_fqdn', '5432', 'db_name', 'db_user', 'db_user_passwd', 'app_name','debug')
logger.InsertLog("info","Test","Test.function", os.getpid(), 0)
```

