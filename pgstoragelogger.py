#!/bin/python3

import psycopg2
from datetime import datetime
import uuid
import socket

class PGStorageLogger:
    def __init__(self, dbhost, dbport, dbname, dbuser, dbpasswd, appname, log_level):
        """
        Инициализация класса логирования (сразу будет создана таблица для логов!!!)
        str(dbhost) - IP - адрес или FQDN имя сервера базы данных PostgreSQL.
        str(dbport) - Порт сервера базы данных PostgreSQL.
        str(dbname) - Имя базы данных для подключения к БД, для создания таблиц в схеме public и добавление записей в такие таблицы.
        str(dbuser) - Имя пользователя для подключения к БД, для создания таблиц в схеме public и добавление записей в такие таблицы.
        str(dbpasswd) - Пароль пользователя для подключения к БД, для создания таблиц в схеме public и добавление записей в такие таблицы.
        str(appname) - Необязательное значение. Сохраняет в таблицу с логами имя приложения, которые будет указано.
        str(log_level) - Возможные значения: 'crit', 'error', 'warning', 'info', 'debug'. Уровень логирования.
        """
        if log_level == "crit":
            self.log_level = 4
        elif log_level == "error":
            self.log_levele = 3
        elif log_level == "warning":
            self.log_level = 2
        elif log_level == "info":
            self.log_level = 1
        elif log_level == "debug":
            self.log_level = 0
        else:
            self.log_level = -1
        self.hostname = str(socket.gethostname())
        self.appname = appname
        self.uuid = str(uuid.uuid4())
        try:
            self.connection = psycopg2.connect(user=dbuser,
                                        password=dbpasswd,
                                        host=dbhost,
                                        port=dbport,
                                        database=dbname,
                                        application_name = self.hostname+': PGStorageLogger.Init')
        except (Exception) as error:
            return "Ошибка при открытии соединения с PostgreSQL: {}".format(error)
        self.section_name = self.GetSection(0)
        if self.section_name == None:
            self.NextNewSection()
            self.GetSection(0)
       
    def GetSection(self, curr_or_next):
        """
        0 - current
        1 - next
        return "0_log_2022_02_17" or None
        """
        """Получаем список всех таблиц"""
        cursor = self.connection.cursor()
        cursor.execute("SET application_name = %(pg_app_name)s", {"pg_app_name":'{}: PGStorageLogger.GetSection'.format(self.hostname)})
        sql = "SELECT tablename FROM pg_catalog.pg_tables WHERE tablename LIKE '%_log_%';"
        cursor.execute(sql)
        self.section = cursor.fetchall()
        self.connection.commit()
        cursor.close()
        """Выбираем ближайшее по дате и последнее по первому индексу"""
        self.section.sort()
        """Если секция для логов в принципе не оказалось"""
        try:
            self.section = self.section[len(self.section)-1][0]
        except:
            self.section = None
        """Обрабатываем curr_or_next: запрашивают текущую секцию или следуюущую"""
        if curr_or_next == 0:
            return self.section
        elif curr_or_next == 1:
            if self.section == None:
                return '_log_'+str(datetime.now().date()).replace("-", "_")+'_'+str(0)
            i = []
            count = len(self.section)-1
            while self.section[count] != '_':
                i.insert(0,self.section[count])
                count -= 1     
            i = int(''.join(map(str, i))) + 1
            return '_log_'+str(datetime.now().date()).replace("-", "_")+'_'+str(i)
        else:
            return None

    def NextNewSection(self):
        """
        Создает таблицу для логов
        return True
        """
        section_name = self.GetSection(1)
        cursor = self.connection.cursor()
        cursor.execute("SET application_name = %(pg_app_name)s", {"pg_app_name":'{}: PGStorageLogger.NextNewSection'.format(self.hostname)})
        sql = """CREATE TABLE public.{section_name}
            (
            id bigserial,
            uuid uuid,
            pid integer,
            pgpid integer, 
            type text, 
            message text, 
            functionname text, 
            appname text, 
            hostname text, 
            "timestamp" timestamp without time zone, 
            CONSTRAINT pk_{section_name}_id PRIMARY KEY (id)
            ) 
            WITH (
            OIDS = FALSE
            )
            ;""".format(section_name = section_name)
        cursor.execute(sql)
        self.connection.commit()
        cursor.close()
        self.section_name = section_name
        return True
    def InsertLog(self, type, message, functionname, pid, pgpid):
        """
        Вставляет запись в таблицу логов
        str(type) - тип события (debug, info, warning, error, crit)
        str(message) - Сообщение, в котором описывается событие
        str(functionname) - метод, в котором происходит логирование
        int(pid) - необязательно, можно внести в таблицу логов пид, например с помощью os.getpid()
        int(pgpid) - необяательно, можно внести в таблицу логов пид postgres, например с помощью psycopg2.connection.get_backend_pid()

        return int(id)
        """
        if type == "crit":
            int_type = 4
        elif type == "error":
            int_type = 3
        elif type == "warning":
            int_type = 2
        elif type == "info":
            int_type = 1
        elif type == "debug":
            int_type = 0
        else:
            int_type = -1
        if self.log_level <= int_type:
            cursor = self.connection.cursor()
            cursor.execute("SET application_name = %(pg_app_name)s", {"pg_app_name":'{}: PGStorageLogger.InsertLog'.format(self.hostname)})
            try:
                cursor.execute("""INSERT INTO public.{} (
                                    uuid, 
                                    type, 
                                    message, 
                                    functionname, 
                                    appname, 
                                    hostname, 
                                    timestamp,
                                    pid,
                                    pgpid
                                    ) 
                                    VALUES (
                                        %(uuid)s,
                                        %(type)s,
                                        %(message)s,
                                        %(functionname)s,
                                        %(appname)s,
                                        %(hostname)s,
                                        %(timestamp)s,
                                        %(pid)s,
                                        %(pgpid)s
                                         ) RETURNING id;""".format(self.section_name), { 
                                                "uuid":self.uuid, "type":type, 
                                                "message":message, 
                                                "functionname":functionname, 
                                                "appname":self.appname, 
                                                "hostname":self.hostname, 
                                                "timestamp":datetime.now(),
                                                "pid":pid,
                                                "pgpid":pgpid})
            except psycopg2.errors.UndefinedTable:
                self.connection.rollback()
                self.NextNewSection()
                self.InsertLog(type, message, functionname)
            id = cursor.fetchone()
            cursor.close()
            self.connection.commit()
            return int(id[0])



